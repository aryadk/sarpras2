<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">
</head>
<body>
	<nav class="navbar navbar-inverse" style="background-color: #555">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href=""></a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="{{url('/beranda')}}">Beranda</a></li>
				<li><a href="{{url('/inventaris')}}">Inventaris</a></li>
				<li><a href="{{url('/peminjaman')}}">Peminjaman</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{url('logout')}}">Logout</a></li>
			</ul>
		</div>	
	</nav>
	<div class="container">
		<h1><b style="color: blue">|</b>Form Inventaris</h1><br><br>
		<div class="well col-sm-6">
			<form method="POST" action="{{url('tambahInvLog')}}" >{{csrf_field()}}
				<div class="form-group">
					<label class="control-label">Kode</label>
					<input type="text" name="kode" class="form-control" required>
				</div>
				<div class="form-group">
					<label class="control-label">Nama Barang</label>
					<input type="text" name="nama" class="form-control" required>
				</div>
				<div class="form-group">
					<label class="control-label">Kondisi</label>
					<select name="kondisi" class="form-control" required>
						<option></option>
						<option value="baik">Baik</option>
						<option value="rusak">Rusak</option>
					</select>
				</div>
				<div class="form-group">
					<label class="control-label">Keterangan</label>
					<input type="text" name="keterangan" class="form-control" required>
				</div>
				<div class="form-group">
					<label class="control-label">Stok</label>
					<input type="number" name="stok" class="form-control" required>
				</div>
				<div class="form-group">
					<label class="control-label">Jenis</label>
					<select name="jenis" class="form-control" required>
						<option></option>
						@foreach($jenis as $jenis):
						<option value="{{$jenis->id}}">{{$jenis->nama}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label class="control-label">Ruang</label>
					<select name="ruang" class="form-control" required>
						<option></option>
						@foreach($ruang as $ruang):
						<option value="{{$ruang->id}}">{{$ruang->kode}}</option>
						@endforeach
					</select>
				</div>
				<br>
				<div class="form-group text-right">
					<button class="btn btn-success" type="submit">Simpan</button>
				</div>
			</form>
		</div>
			
	</div>

</body>
</html>