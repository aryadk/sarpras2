<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">
</head>
<body>
	<nav class="navbar navbar-inverse" style="background-color: #555">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href=""></a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="{{url('/beranda')}}">Beranda</a></li>
				<li><a href="{{url('/inventaris')}}">Inventaris</a></li>
				@if(!Session::get('peminjam')):
				<li><a href="{{url('/peminjaman')}}">Peminjaman</a></li>
				@else:
				<li><a href="{{url('/peminjaman_peminjam')}}">Peminjaman</a></li>
				@endif
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{url('logout')}}">Logout</a></li>
			</ul>
		</div>	
	</nav>

	<div class="container">
		<h1><b style="color: blue">|</b>Data Inventaris</h1><br><br>
		<div class="well">
			@if(Session::get('admin')):
			<div class="row">
				<a href="{{url('tambahInv')}}" class="col-sm-6"><button class="btn btn-success">Tambah</button></a>
				<a href="{{url('/laporanInv')}}" class="col-sm-6 text-right"><button class="btn btn-danger">Buat Laporan</button></a>
			</div><br>@endif
			<table class="table table-stripped">
				<thead>
					<tr>
						<th>No</th>
						<th>Kode</th>
						<th>Nama</th>
						<th>Kondisi</th>
						<th>Keterangan</th>
						<th>stok</th>
						<th>jenis</th>
						<th>Ruang</th>
						<th>Tanggal Regiser</th>
						<th>Petugas</th>
						@if(Session::get('admin'))
						<th>Action</th>
						@endif
					</tr>	
				</thead>
				<tbody>
					@foreach($data as $data):
					<tr>
						<td>{{$no++}}</td>
						<td>{{$data->kode}}</td>
						<td>{{$data->nama}}</td>
						<td>{{$data->kondisi}}</td>
						<td>{{$data->keterangan}}</td>
						<td>{{$data->stok}}</td>
						<td>{{$data->jenis}}</td>
						<td>{{$data->ruang}}</td>
						<td>{{$data->tanggal_regis}}</td>
						<td>{{$data->nama_petugas}}</td>
						@if(Session::get('admin'))
						<td>
							<a href="{{url('editInv/'.$data->id)}}" class="btn btn-warning"><i class="glyphicon glyphicon-pencil"></i></a>
							<a href="{{url('tambahStok/'.$data->id)}}" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i></a>
							<a href="{{url('kurangStok/'.$data->id)}}" class="btn btn-default"><i class="glyphicon glyphicon-minus"></i></a>
						</td>
						@endif
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
			
	</div>
</body>
</html>