<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
</head>
<body>
	<div class="container " style="margin-top: 100px;">
		<div class="col-sm-4"></div>
		<div class="well col-sm-4">
			<h3><b style="color: blue">|</b>Login Operator</h3><br>
			<form method="Post" action="{{url('login_petugasPost')}}">
				{{csrf_field()}}
				<div class="form-group">
					<label class="control-label">Username</label>
					<input type="text" name="username" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label">Password</label>
					<input type="password" name="password" class="form-control">
				</div>
				<div class="form-group text-right">
					<button class="btn btn-success col-sm-12" type="submit">Login</button>
				</div>
			</form>
		</div>
			
	</div>
</body>
</html>