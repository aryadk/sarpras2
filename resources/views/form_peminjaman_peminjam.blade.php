<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">
</head>
<body>
	<nav class="navbar navbar-inverse" style="background-color: #555">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href=""></a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="{{url('/beranda')}}">Beranda</a></li>
				<li><a href="{{url('/inventaris')}}">Inventaris</a></li>
				<li><a href="{{url('/peminjaman')}}">Peminjaman</a></li></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{url('logout')}}">Logout</a></li>
			</ul>
		</div>	
	</nav>
	<div class="container">
		<h1><b style="color: red">|</b>Form Peminjaman</h1><br><br>
		<div class="well col-sm-6">
			<form method="POST" action="{{url('tambahPemPegLog')}}" >
				{{csrf_field()}}
				<div class="form-group">
					<label class="control-label">Nama Barang</label>
					<input type="input" name="barang" class="form-control" required>
				</div>
				<div class="form-group">
					<label class="control-label">Jumlah</label>
					<input type="number" name="jumlah" class="form-control" required>
				</div><br>
				<div class="form-group text-right">
					<button class="btn btn-success" type="submit">Simpan</button>
				</div>
			</form>
		</div>
			
	</div>
</body>
</html>