<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<style type="text/css">
		body{
			background-image: url('assets/img/smk4.jpeg');
			background-position: -290px -160px;
			background-size: 1650px 1150px;
			font-family: Times New Roman;
		}
		.well{
			z-index: -999;
			position: absolute;
			margin-top: -100px;
			width: 50%;
			height: 100%;
			margin-left: 50%;
			background-color: rgba(0,0,0,0.5);
			border: none;
			padding-top: 230px;
		}
		h1{
			line-height: 1.5;
			color:#fefefe;
			font-size: 40pt;
		}
		li a{
			color: #fff;
			font-size: 13pt;
		}
		li a:hover{
			color: #222;
			font-size: 13pt;
			background-color: #eee;
		}
		#logout{
			color: #fff;
			background-color: #ff0000;
			font-size: 13pt;
		}
		#logout:hover{
			color: #ff0000;
			background-color: #eee;
		}
	</style>
</head>
<body>
	<nav class="navbar">
		<div class="container">
			<div class="navbar-header">
				<a href=""></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{url('/beranda')}}">Beranda</a></li>
				<li><a href="{{url('/inventaris')}}">Inventaris</a></li>
				@if(!Session::get('peminjam')):
				<li><a href="{{url('/peminjaman')}}">Peminjaman</a></li>
				@else:
				<li><a href="{{url('/peminjaman_peminjam')}}">Peminjaman</a></li>
				@endif


				
				<li><a href="{{url('/logout')}}" id="logout">Logout</a></li>
			</ul>
		</div>	
		
	</nav>
		<div class="well text-right">
			<div class="container-fluid">
				<h1>SISTEM APLIKASI <br>SARANA PRASARANA</h1>
			</div>
		</div>
</body>
</html>