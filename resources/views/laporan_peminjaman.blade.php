<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">
</head>
<body>
	<nav class="navbar navbar-inverse" style="background-color: #555">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href=""></a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="{{url('/beranda')}}">Beranda</a></li>
				<li><a href="{{url('/inventaris')}}">Inventaris</a></li>
				<li><a href="{{url('/peminjaman')}}">Peminjaman</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{url('logout')}}">Logout</a></li>
			</ul>
		</div>	
	</nav>
	<div class="container">
		<h1>Laporan Peminjaman Tahun {{date('Y')}}</h1><br><br>
			<table class="table">
				<thead>
					<tr>
						<th>No</th>
						<th>ID</th>
						<th>Nama</th>
						<th>Nama Barang</th>
						<th>jumlah</th>
						<th>Tanggal Pinjam</th>
						<th>Tanggal Kembali</th>
						<th>Status</th>
					</tr>	
				</thead>
				<tbody>
					@foreach($data as $data):
					<tr>
						<td>{{$no++}}</td>
						<td>{{$data->id}}</td>
						<td>{{$data->nama}}</td>
						<td>{{$data->nama_barang}}</td>
						<td>{{$data->jumlah}}</td>
						<td>{{$data->tanggal_pinjam}}</td>
						<td>{{$data->tanggal_kembali}}</td>
						<td>{{$data->status}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			
	</div>
</body>
</html>