<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">
</head>
<body>
	<nav class="navbar navbar-inverse" style="background-color: #555">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href=""></a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="{{url('/beranda')}}">Beranda</a></li>
				<li><a href="{{url('/inventaris')}}">Inventaris</a></li>
				<li><a href="{{url('/peminjaman')}}">Peminjaman</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{url('logout')}}">Logout</a></li>
			</ul>
		</div>	
	</nav>
	@yield('konten')
</body>
</html>