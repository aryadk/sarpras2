<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">
</head>
<body>
	<nav class="navbar navbar-inverse" style="background-color: #555">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href=""></a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="{{url('/beranda')}}">Beranda</a></li>
				<li><a href="{{url('/inventaris')}}">Inventaris</a></li>
				<li><a href="{{url('/peminjaman')}}">Peminjaman</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{url('logout')}}">Logout</a></li>
			</ul>
		</div>	
	</nav>

	<div class="container">
		<h1>Laporan Inventaris per Bulan {{date("M Y")}}</h1><br><br>
			<table class="table table-stripped">
				<thead>
					<tr>
						<th>No</th>
						<th>Kode</th>
						<th>Nama</th>
						<th>Kondisi</th>
						<th>Keterangan</th>
						<th>stok</th>
						<th>jenis</th>
						<th>Ruang</th>
						<th>Tanggal Regiser</th>
						<th>Petugas</th>
					</tr>	
				</thead>
				<tbody>
					@foreach($data as $data):
					<tr>
						<td>{{$no++}}</td>
						<td>{{$data->kode}}</td>
						<td>{{$data->nama}}</td>
						<td>{{$data->kondisi}}</td>
						<td>{{$data->keterangan}}</td>
						<td>{{$data->stok}}</td>
						<td>{{$data->jenis}}</td>
						<td>{{$data->ruang}}</td>
						<td>{{$data->tanggal_regis}}</td>
						<td>{{$data->nama_petugas}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			
	</div>
</body>
</html>