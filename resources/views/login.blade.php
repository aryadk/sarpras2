<!DOCTYPE html>
<html>
	
<head>
	<title></title>

	
	<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">
	<style type="text/css">
		a{
			color: #ff0000;
		}
		a:hover{
			color: #ff0000;
		}
		img{
			width:300px;
			height:auto;
		}
	</style>
</head>
<body>
	<div class="container text-center"><br><br>
		<img src="{{url('assets/img/logo.png')}}">
		<h2>Masuk Sebagai?</h2>
		<a href="{{url('login_admin')}}">Administrator</a> | <a href="{{url('login_operator')}}">Operator</a> | <a href="{{url('login_peminjam')}}">Peminjam/Pegawai</a>
	</div>
</body>
</html>