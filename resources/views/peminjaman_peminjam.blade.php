<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">
</head>
<body>
	<nav class="navbar navbar-inverse" style="background-color: #555">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href=""></a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="{{url('/beranda')}}">Beranda</a></li>
				<li><a href="{{url('/inventaris')}}">Inventaris</a></li>
				@if(Session::get('peminjam')):
				<li><a href="{{url('/peminjaman')}}">Peminjaman</a></li>
				@else:
				<li><a href="{{url('/peminjaman_peminjam')}}">Peminjaman</a></li>
				@endif
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{url('logout')}}">Logout</a></li>
			</ul>
		</div>	
	</nav>
	<div class="container">
		<h1><b style="color: red">|</b>Data Peminjaman</h1><br><br>
		<div class="well">
			<div class="row">
				<a href="{{url('tambahPemPeg')}}" class="col-sm-6"><button class="btn btn-success">Pinjam Barang</button></a>
			</div><br>
			<h4>Atas Nama: {{$pegawai->nama}}</h4>
			<h5>Barang yang belum dikembalikan:</h5>
			<table class="table">
				<thead>
					<tr>
						<th>No</th>
						<th>ID</th>
						<th>Nama</th>
						<th>Nama Barang</th>
						<th>jumlah</th>
						<th>Tanggal Pinjam</th>
						<th>Status</th>
					</tr>	
				</thead>
				<tbody>
					@foreach($data as $data):
					<tr>
						<td>{{$no++}}</td>
						<td>{{$data->id}}</td>
						<td>{{$data->nama}}</td>
						<td>{{$data->nama_barang}}</td>
						<td>{{$data->jumlah}}</td>
						<td>{{$data->tanggal_pinjam}}</td>						
						<td>{{$data->status}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
			
	</div>
</body>
</html>