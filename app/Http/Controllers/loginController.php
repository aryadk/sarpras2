<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Petugas;
use App\Pegawai;
class loginController extends Controller
{
   function index(){
      return view('login');
   }
   function petugasPost(Request $request){
      $username = $request->username;
      $password = md5($request->password);
      $data = Petugas::where('username',$username)->first();
      if($data != null){
         if($data->password == $password){
            if ($data->id_level == 1) {
               Session::put('admin',TRUE);
               Session::put('id_petugas',$data->id);
               return redirect('beranda');
            }elseif($data->id_level == 2){
               Session::put('operator',TRUE);
               Session::put('id_petugas',$data->id);
               return redirect('beranda');
            }else{
               Session::put('peminjam',TRUE);
               Session::put('id_petugas',$data->id);
               return redirect('beranda');
            }
         }
         else{
            return redirect('')->with('alert','Password Salah');;
         }
      }
      else{
         return redirect('')->with('alert','User Tidak Ditemukan');;
      }
   }
   function pegawaiPost(Request $request){
      $username = $request->username;
      $password = md5($request->password);
      $data = Pegawai::where('username',$username)->first();
      if($data != null){
         if($data->password == $password){
            if ($data->id_level == 1) {
               Session::put('admin',TRUE);
               Session::put('id_pegawai',$data->id);
               return redirect('beranda');
            }elseif($data->id_level == 2){
               Session::put('operator',TRUE);
               Session::put('id_pegawai',$data->id);
               return redirect('beranda');
            }else{
               Session::put('peminjam',TRUE);
               Session::put('id_pegawai',$data->id);
               return redirect('beranda');
            }
         }
         else{
            return redirect('')->with('alert','Password Salah');
         }
      }
      else{
         return redirect('')->with('alert','User tidak ditemukan ');;
      }
   }
   function logout(){
      Session::flush();
      return redirect('');
   }
}
