<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Inventaris;
use App\Jenis;
use App\Ruang;

class inventarisController extends Controller
{
    function index(){
    	$id_petugas = Session::get('id_petugas');
	   	$data = Inventaris::
	   	join('jenis','jenis.id','=','inventaris.id_jenis')
	   	->join('ruang','ruang.id','=','inventaris.id_ruang')
	   	->join('petugas','petugas.id','=','inventaris.id_petugas')
	   	->select('inventaris.*','jenis.nama as jenis','ruang.nama as ruang',
	   		'petugas.nama as nama_petugas')
	   	->get();
	   	$no = 1;
	   	return view('inventaris',compact('data','no'));
   }
   function tambahView(){
   		if(!Session::get('admin')){
    		return redirect('');
    	}
	   	$jenis = Jenis::get();
	   	$ruang = Ruang::get();
	   	return view('form_inventaris',
	   		compact('jenis','ruang'));
   }
   function tambah(Request $request){
   		if(!Session::get('admin')){
    		return redirect('');
    	}
    	$id_petugas = Session::get('id_petugas');
   		$data = new Inventaris;
   		$data->kode = $request->kode;
   		$data->nama = $request->nama;
   		$data->kondisi = $request->kondisi;
   		$data->keterangan = $request->keterangan;
   		$data->stok = $request->stok;
   		$data->id_jenis = $request->jenis;
   		$data->id_ruang = $request->ruang;
   		$data->tanggal_regis = date("Y-m-d");
   		$data->id_petugas = $id_petugas;
   		$data->save();

   		return redirect('inventaris')->with('alert','Barang berhasil diinput');
   }
   function tambahStok($id){
   		$data= Inventaris::where('id',$id)->first();
   		$stokakhir = $data->stok + 1;
   		$update = Inventaris::find($id);
   		$update->stok = $stokakhir;
   		$update->save();

   		return redirect('inventaris');
   }
   function kurangStok($id){
   		$data= Inventaris::where('id',$id)->first();
   		$stokakhir = $data->stok - 1;
   		$update = Inventaris::find($id);
   		$update->stok = $stokakhir;
   		$update->save();

   		return redirect('inventaris');
   }
   function editView($id){
   		$jenis = Jenis::get();
	   	$ruang = Ruang::get();
   		$data = Inventaris::
	   	where('id',$id)
	   	->first();

	   	return view('edit_inventaris',compact('data','jenis','ruang','id'));
   }
   function edit($id, Request $request){
   		$update = Inventaris::find($id);
   		$update->kode = $request->kode;
   		$update->nama = $request->nama;
   		$update->kondisi = $request->kondisi;
   		$update->keterangan = $request->keterangan;
   		$update->stok = $request->stok;
   		$update->id_jenis = $request->jenis;
   		$update->id_ruang = $request->ruang;
   		$update->save();

   		return redirect('inventaris');
   }
   function laporan(){
   		if(!Session::get('admin')){
    		return redirect('');
    	}
    	$id_petugas = Session::get('id_petugas');
	   	$data = Inventaris::
	   	join('jenis','jenis.id','=','inventaris.id_jenis')
	   	->join('ruang','ruang.id','=','inventaris.id_ruang')
	   	->join('petugas','petugas.id','=','inventaris.id_petugas')
	   	->select('inventaris.*','jenis.nama as jenis','ruang.nama as ruang',
	   		'petugas.nama as nama_petugas')
	   	->get();
	   	$no = 1;
	   	return view('laporan_inventaris',compact('data','no'));
   }
}
