<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Peminjaman;
use App\Petugas;
use App\Inventaris;
use App\DetailPeminjaman;
use App\Pegawai;

class peminjamanController extends Controller
{
    function index(){
        if(Session::get('peminjam')){
            return redirect('');
        }
        $no=1;
        $data = Peminjaman::join('detail_peminjaman','detail_peminjaman.id_peminjaman','=','peminjaman.id')
        ->join('pegawai','pegawai.id','=','peminjaman.id_pegawai')
        ->join('inventaris','inventaris.id','=','detail_peminjaman.id_inventaris')
        ->select('peminjaman.*','pegawai.nip as nip','pegawai.nama as nama','inventaris.nama as nama_barang','detail_peminjaman.jumlah as jumlah')
        ->orderBy('peminjaman.id','desc')
        ->get();
        return view('peminjaman',compact('data','no'));
    }
     function indexPem(){
        if(!Session::get('peminjam')){
            return redirect('');
        }
        $id_pegawai = Session::get('id_pegawai');
        $no=1;
        $pegawai = Pegawai::where('id',$id_pegawai)->first();
        $data = Peminjaman::join('detail_peminjaman','detail_peminjaman.id_peminjaman','=','peminjaman.id')
        ->join('pegawai','pegawai.id','=','peminjaman.id_pegawai')
        ->join('inventaris','inventaris.id','=','detail_peminjaman.id_inventaris')
        ->where('peminjaman.id_pegawai',$id_pegawai)
        ->where('peminjaman.status','belum dikembalikan')
        ->select('peminjaman.*','pegawai.nip as nip','pegawai.nama as nama','inventaris.nama as nama_barang','detail_peminjaman.jumlah as jumlah')
        ->orderBy('peminjaman.id','desc')
        ->get();
        return view('peminjaman_peminjam',compact('data','no','pegawai'));
    }
    function tambahView(){
        return view('form_peminjaman');
    }
    function tambah(Request $request){
        $nip = $request->nip;
        $nama = $request->nama;
        $barang = $request->barang;
        $jumlah = $request->jumlah;

        $pegawai = Pegawai::where('nip',$nip)->first();

        $data = new Peminjaman;
        $data->tanggal_pinjam = date("Y-m-d");
        $data->id_pegawai = $pegawai->id;
        $data->save();

        $peminjaman = Peminjaman::where('id_pegawai',$pegawai->id)->orderBy('id','desc')->first();
        $inventaris = Inventaris::where('nama',$barang)->first();

        $data1 = new DetailPeminjaman;
        $data1->id_inventaris = $inventaris->id;
        $data1->id_peminjaman = $peminjaman->id;
        $data1->jumlah = $jumlah;
        $data1->save();

        $stokakhir = $inventaris->stok - $jumlah;
        $update = Inventaris::find($inventaris->id);
        $update->stok = $stokakhir;
        $update->save();

        return redirect('peminjaman');
    }
    function tambahPegView(){
        return view('form_peminjaman_peminjam');
    }
    function tambahPeg(Request $request){
        $id_pegawai = Session::get('id_pegawai');
        $barang = $request->barang;
        $jumlah = $request->jumlah;


        $data = new Peminjaman;
        $data->tanggal_pinjam = date("Y-m-d");
        $data->id_pegawai = $id_pegawai;
        $data->save();

        $peminjaman = Peminjaman::where('id_pegawai',$id_pegawai)->orderBy('id','desc')->first();
        $inventaris = Inventaris::where('nama',$barang)->first();

        $data1 = new DetailPeminjaman;
        $data1->id_inventaris = $inventaris->id;
        $data1->id_peminjaman = $peminjaman->id;
        $data1->jumlah = $jumlah;
        $data1->save();

        $stokakhir = $inventaris->stok - $jumlah;
        $update = Inventaris::find($inventaris->id);
        $update->stok = $stokakhir;
        $update->save();

        return redirect('peminjaman_peminjam');
    }
    function pengembalian($id){
        $data = Peminjaman::join('detail_peminjaman','detail_peminjaman.id_peminjaman','=','peminjaman.id')
        ->join('pegawai','pegawai.id','=','peminjaman.id_pegawai')
        ->join('inventaris','inventaris.id','=','detail_peminjaman.id_inventaris')
        ->where('detail_peminjaman.id_peminjaman',$id)
        ->select('peminjaman.*','pegawai.nip as nip','pegawai.nama as nama','inventaris.stok as stok','detail_peminjaman.jumlah as jumlah','detail_peminjaman.id_inventaris as id_inv')
        ->first();

        $stokakhir = $data->jumlah + $data->stok;

        $update = Peminjaman::find($id);
        $update->status = 'sudah kembali';
        $update->tanggal_kembali = date("Y-m-d");
        $update->save();

        $update1 = Inventaris::find($data->id_inv);
        $update1->stok = $stokakhir;
        $update1->save();

        return redirect('peminjaman');
    }
    function laporanPem(){
        if(!Session::get('admin')){
            return redirect('');
        }
        $no=1;
        $data = Peminjaman::join('detail_peminjaman','detail_peminjaman.id_peminjaman','=','peminjaman.id')
        ->join('pegawai','pegawai.id','=','peminjaman.id_pegawai')
        ->join('inventaris','inventaris.id','=','detail_peminjaman.id_inventaris')
        ->select('peminjaman.*','pegawai.nip as nip','pegawai.nama as nama','inventaris.nama as nama_barang','detail_peminjaman.jumlah as jumlah')
        ->get();
        return view('laporan_peminjaman',compact('data','no'));
    }
    function delete($id){
        $data = Peminjaman::find($id);
        $data->delete(); 
        return redirect('peminjaman');
    }
}
