<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventaris extends Model
{
    protected $table = 'inventaris';
    protected $fillable = 
    ['kode','nama','kondisi','stok','keterangan','stok','id_jenis','tanggal_regis','id_ruang','id_petugas'];
}
