<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'jenis';
    protected $fillable = 
    ['nama'];
}
