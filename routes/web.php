<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('/login_admin', function () {
    return view('login_admin');
});
Route::get('/login_operator', function () {
    return view('login_operator');
});
Route::get('/login_peminjam', function () {
    return view('login_peminjam');
});
Route::post('/login_petugasPost', 'loginController@petugasPost');
Route::post('/login_pegawaiPost', 'loginController@pegawaiPost');
Route::get('/logout', 'loginController@logout');

Route::get('/beranda', function () {
    return view('beranda');
});
Route::get('/inventaris', 'InventarisController@index');
Route::get('/tambahInv', 'InventarisController@tambahView');
Route::post('/tambahInvLog', 'InventarisController@tambah');
Route::get('/editInv/{id}', 'InventarisController@editView');
Route::get('/tambahStok/{id}', 'InventarisController@tambahStok');
Route::get('/laporanInv', 'InventarisController@laporan');
Route::get('/kurangStok/{id}', 'InventarisController@kurangStok');
Route::post('editInvLog/{id}', 'InventarisController@edit');

Route::get('/peminjaman', 'peminjamanController@index');
Route::get('/peminjaman_peminjam', 'peminjamanController@indexPem');
Route::get('/tambahPem', 'peminjamanController@tambahView');
Route::post('/tambahPemLog', 'peminjamanController@tambah');
Route::get('/tambahPemPeg', 'peminjamanController@tambahPegView');
Route::post('/tambahPemPegLog', 'peminjamanController@tambahPeg');
Route::get('/deletePem/{id}', 'peminjamanController@delete');
Route::get('/pengembalian/{id}', 'peminjamanController@pengembalian');
Route::get('/laporanPem', 'peminjamanController@laporanPem');
